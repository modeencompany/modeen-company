Modeen Company is a factory-trained and certified dealer and installer of quality windows and doors, including Weather Shield, H-Window, Sunrise Windows, Albany Doors, Genius Retractable Screens, and more.

Address: 1285 114th Avenue NW, Suite 140, Coon Rapids, MN 55448

Phone: 763-425-3600
